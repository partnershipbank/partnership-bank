Bank easy at Partnership Bank. It is our mission to make banking easy for our clients AND take care of the people and businesses who call Wisconsin home. Whether buying a home, growing a business, or saving for a rainy day, we want to be your partner and bank.

Address: W61 N529 Washington Avenue, Cedarburg, WI 53012

Phone: 262-377-3800